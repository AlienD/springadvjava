package kz.aitu.advJava.controller;

import kz.aitu.advJava.model.Student;
import kz.aitu.advJava.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    private final  StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/api/students")
    public ResponseEntity<?> getStudents(){
        return ResponseEntity.ok(studentRepository.findAll());
    }

    @GetMapping("/api/group1")
    public ResponseEntity<?> getStudentsGroup1(){
        return ResponseEntity.ok(studentRepository.getStudentGroup1());
    }

    @GetMapping("/api/students/{groupid}")
    public ResponseEntity<?> getStudentsByGroupId(@PathVariable int groupid){
        return ResponseEntity.ok(studentRepository.findAllByGroupid(groupid));
    }
}
