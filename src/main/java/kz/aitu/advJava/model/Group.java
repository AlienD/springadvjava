package kz.aitu.advJava.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Group {
    private int id;
    private String name;

}
